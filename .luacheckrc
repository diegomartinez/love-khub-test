
self = false

read_globals = {
	"love",
}

exclude_files = {
	"lib/khub/",
	"lib/klass/",
	"lib/simpleui/",
}

files["lib/app/main.lua"] = {
	globals = { "love" },
}

files["lib/app/thread/Thread.lua"] = {
	globals = { "love" },
}
