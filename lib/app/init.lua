
---
-- TODO: Doc.
--
-- @module app
-- @see app.ui
-- @see app.thread

local app = { }

app.ui = require "app.ui"
app.thread = require "app.thread"

return app
