
---
-- Simple text console widget.
--
-- **Extends:** `simpleui.Widget`
--
-- @classmod app.ui.Console

local Widget = require "simpleui.Widget"

local Console = Widget:extend("app.ui.Console")

local gfx = love.graphics

---
-- Output buffer.
--
-- @tfield table buffer Default is an empty table.
Console.buffer = nil

---
-- TODO: Doc.
--
-- @tfield love.font.Font font Default is nil.
Console.font = nil

---
-- Constructor.
--
-- @tparam table params Field overrides.
function Console:init(params)
	self.buffer = params and params.buffer or { }
	return Widget.init(self, params)
end

---
-- Output some values.
--
-- @tparam any ... Values to output.
function Console:print(...)
	local n, t = select("#", ...), { ... }
	for i = 1, n do
		t[i] = tostring(t[i])
	end
	local str = table.concat(t, " ")
	local pos, endp = 1, #str+1
	repeat
		local s, e = str:find("\n", pos, true)
		self.buffer[#self.buffer+1] = str:sub(pos, s and s - 1)
		pos = e and e + 1
	until (not pos) or pos > endp
end

---
-- Output a formatted string.
--
-- @tparam string fmt Format string.
-- @tparam any ... Format arguments.
function Console:printf(fmt, ...)
	return self:print(fmt:format(...))
end

function Console:paintfg()
	local oldfont = gfx.getFont()
	local font = self.font or oldfont
	local lineh = font:getHeight("Ay")
	local lines = math.ceil(self.h / lineh)
	local len = #self.buffer
	local yy = math.min(self.h, math.floor(len*lineh))
	local clip = { gfx.getScissor() }
	local ax, ay = self:abspos()
	gfx.setFont(font)
	gfx.setScissor(ax, ay, self.w, self.h)
	for i = len, math.max(1, len-lines+1), -1 do
		yy = yy - lineh
		gfx.print(self.buffer[i], 0, yy)
	end
	gfx.setScissor(unpack(clip))
	gfx.setFont(oldfont)
end

return Console
