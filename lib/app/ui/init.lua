
---
-- User interface elements.
--
-- @module app.ui
-- @see app.ui.Console

local ui = { }

ui.Console = require "app.ui.Console"

return ui
