
---
-- TODO: Doc.
--
-- @module app.timer

local timer = { }

local timers = { }

---
-- TODO: Doc.
--
-- @tparam number delay
-- @tparam function func
-- @tparam any ...
function timer.after(delay, func, ...)
	local args = { n=select("#", ...), ... }
	local time = love.timer.getTime()+delay
	timers[#timers+1] = {
		func = func,
		args = args,
		time = time,
	}
end

---
-- TODO: Doc.
--
function timer.poll()
	local now = love.timer.getTime()
	local i, l = 1, #timers
	while i <= l do
		local t = timers[i]
		if t.time < now then
			print("after:", t.time)
			table.remove(timers, i)
			l = l - 1
			t.func(unpack(t.args, 1, t.args.n))
		else
			i = i + 1
		end
	end
end

return timer
