
---
-- TODO: Doc.
--
-- **Extends:** `klass`
--
-- @classmod app.thread.Thread

local serial = require "khub.serial"
local klass = require "klass"
local Thread = klass:extend("app.thread.Thread")

local threads = setmetatable({ }, { __mode="kv" })

---
-- TODO: Doc.
--
-- @tfield table commands Default is an empty table.
Thread.commands = ({ })

---
-- Constructor.
--
function Thread:init()
	self.commands = setmetatable({ }, { __index=self.commands })
	self.input = love.thread.newChannel()
	self.output = love.thread.newChannel()
	self.thread = love.thread.newThread([[
		local function run(name, input, output, ...)
			local C = require(name)
			local inst = setmetatable({ }, C)
			inst.input = input
			inst.output = output
			inst.alive = true
			return inst:run(...)
		end
		return run(...)
	]])
	threads[self.thread] = self
end

function love.threaderror(thr, err)
	thr = threads[thr]
	if thr then
		return thr:onerror(err)
	end
	return error(err)
end

---
-- TODO: Doc.
--
-- @tparam any ...
function Thread:start(...)
	return assert(self.thread):start(
			self.__name, self.output, self.input, ...)
end

---
-- TODO: Doc.
--
-- @tparam any ...
function Thread:run()
	repeat
		self:step()
	until not self:isalive()
end

---
-- TODO: Doc.
--
function Thread:step()
	return self:handlecommand(self:receive())
end

---
-- TODO: Doc.
--
-- @tparam string cmd
-- @tparam any ...
function Thread:handlecommand(cmd, ...)
	if cmd == nil then
		return
	end
	local func = self.commands[cmd]
	if func then
		return func(self, ...)
	end
	return self:onunknowncommand(cmd, ...)
end

---
-- TODO: Doc.
--
function Thread:stop()
	if self.thread then
		self:send("_Thread_stop")
	end
end

function Thread.commands:_Thread_stop()
	self.alive = false
end

---
-- TODO: Doc.
--
function Thread:receive()
	local data = self.input:pop()
	if data then
		local function bail(ok, ...)
			assert(ok)
			return ...
		end
		return bail(serial.load(data))
	end
end

---
-- TODO: Doc.
--
function Thread:send(...)
	return self.output:push(serial.dump(...))
end

---
-- TODO: Doc.
--
function Thread:ischild()
	return self.thread == nil
end

---
-- TODO: Doc.
--
function Thread:isalive()
	if self.thread then
		return self.thread:isRunning()
	else
		return self.alive
	end
end

---
-- TODO: Doc.
--
function Thread:wait()
	if self.thread then
		return self.thread:wait()
	end
end

---
-- TODO: Doc.
--
-- @tparam string err
function Thread:onerror(err) -- luacheck: ignore
	return error(err)
end

---
-- TODO: Doc.
--
-- @tparam string cmd
-- @tparam any ...
function Thread:onunknowncommand(cmd, ...) -- luacheck: ignore
end

---
-- TODO: Doc.
--
function Thread:__tostring()
	return (("<%s: %s, %s>"):format(
			self.__name,
			self:ischild() and "child" or "parent",
			self:isalive() and "alive" or "dead"))
end

return Thread
