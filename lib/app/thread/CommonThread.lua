
---
-- TODO: Doc.
--
-- **Extends:** `app.thread.Thread`
--
-- @classmod app.thread.CommonThread

local Thread = require "app.thread.Thread"
local CommonThread = Thread:extend("app.thread.CommonThread")

---
-- TODO: Doc.
--
-- @tfield string host Default is nil.
CommonThread.host = nil

---
-- TODO: Doc.
--
-- @tfield number port Default is 12345.
CommonThread.port = 12345

CommonThread.commands = setmetatable({ }, { __index=Thread.commands })

---
-- TODO: Doc.
--
-- @treturn any conn
function CommonThread:openconn(host, port) -- luacheck: ignore
end

---
-- TODO: Doc.
--
-- @tparam any conn
function CommonThread:closeconn(conn) -- luacheck: ignore
end

function CommonThread:run(host, port)
	self:openconn(host, port)
	Thread.run(self)
	self:closeconn()
end

return CommonThread
