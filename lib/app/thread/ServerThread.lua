
---
-- TODO: Doc.
--
-- **Extends:** `app.thread.CommonThread`
--
-- @classmod app.thread.ServerThread

local server = require "khub.server"

local CommonThread = require "app.thread.CommonThread"
local ServerThread = CommonThread:extend("app.thread.ServerThread")

ServerThread.host = "0.0.0.0"

ServerThread.commands = setmetatable({ }, { __index=CommonThread.commands })

function ServerThread:openconn(host, port)
	local srv = assert(server(host or self.host, port or self.port))

	assert(srv:listen())

	self.server = srv
end

function ServerThread:closeconn()
	self.server:shutdown()
	self.server = nil
end

function ServerThread:step()
	if self.server then
		self.server:step()
	end
	return CommonThread.step(self)
end

return ServerThread
