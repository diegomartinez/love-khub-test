
---
-- TODO: Doc.
--
-- **Extends:** `app.thread.CommonThread`
--
-- @classmod app.thread.ClientThread

local client = require "khub.client"
local socket = require "socket"

local PINGINTERVAL = 15

local CommonThread = require "app.thread.CommonThread"
local ClientThread = CommonThread:extend("app.thread.ClientThread")

---
-- TODO: Doc.
--
-- @tfield khub.client client
ClientThread.client = nil

---
-- TODO: Doc.
--
-- @tfield number time
ClientThread.time = nil

ClientThread.host = "127.0.0.1"

ClientThread.commands = setmetatable({ }, { __index=CommonThread.commands })

---
-- TODO: Doc.
--
-- @tparam khub.clientinfo target
-- @tparam string message
function ClientThread:sendmessage(target, message)
	if not self:ischild() then
		return self:send("sendmessage", target, message)
	end
end

function ClientThread.commands:sendmessage(target, message)
	if self:ischild() then
		return self.client:send("msg", target, message)
	end
end

---
-- TODO: Doc.
--
-- @treturn boolean ok
-- @treturn ?string error
function ClientThread:onwelcome()
end

function ClientThread.commands:welcome(id, name, desc)
	self.id = id
	self.servername = name
	self.serverdesc = desc
	if not self:ischild() then
		return self:onwelcome()
	end
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
-- @treturn boolean ok
-- @treturn ?string error
function ClientThread:onuserjoin(user) -- luacheck: ignore
end

function ClientThread.commands:userjoin(...)
	if self:ischild() then
		return self:send("userjoin", ...)
	else
		return self:onuserjoin(...)
	end
end

---
-- TODO: Doc.
--
-- @tparam khub.user user
-- @tparam ?string reason
-- @treturn boolean ok
-- @treturn ?string error
function ClientThread:onuserquit(user, reason) -- luacheck: ignore
end

function ClientThread.commands:userquit(...)
	if self:ischild() then
		return self:send("userquit", ...)
	else
		return self:onuserquit(...)
	end
end

---
-- TODO: Doc.
--
-- @tparam khub.clientinfo user
-- @tparam string message
function ClientThread:onmessage(user, message) -- luacheck: ignore
end

function ClientThread.commands:message(target, message)
	if self:ischild() then
		return self.client:send("message", target, message)
	else
		return self:onmessage(target, message)
	end
end

function ClientThread:openconn(host, port)
	local clt = assert(client(host or self.host, port or self.port))

	clt.onwelcome = function()
		self:send("welcome", clt.id, clt.servername, clt.serverdesc)
	end

	clt.onuserjoin = function(_, user, name)
		self:send("userjoin", user, name)
	end

	clt.onuserquit = function(_, user, reason)
		self:send("userquit", user, reason)
	end

	clt.onmessage = function(_, id, msg)
		self:send("message", id, msg)
	end

	self.time = socket.gettime()

	assert(clt:connect())

	self.client = clt
end

function ClientThread:closeconn()
	self.client:shutdown()
	self.client = nil
end

function ClientThread:step()
	if self.client then
		local now = socket.gettime()
		if now - self.time > PINGINTERVAL then
			self.client:send("ping")
			self:send("sentping")
			self.time = now
		end
		self.client:step()
	end
	return CommonThread.step(self)
end

return ClientThread
