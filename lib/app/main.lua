
local sui = require "simpleui"
local timer = require "app.timer"

local ClientThread = require "app.thread.ClientThread"
local ServerThread = require "app.thread.ServerThread"
local Console = require "app.ui.Console"

local console
local cthr, sthr
local entry

local function loveupdate(dtime)
	timer.poll()
	if sthr then
		sthr:step()
	end
	if cthr then
		cthr:step()
	end
	return sui.update(dtime)
end

local function lovekeypressed(key, ...)
	if key == "esc" then
		return love.event.quit()
	else
		return sui.keypressed(key, ...)
	end
end

local function shutdown()
	if cthr then
		cthr:stop()
		cthr:wait()
		cthr = nil
	end
	if sthr then
		sthr:stop()
		sthr:wait()
		sthr = nil
	end
end

local function lovequit()
	shutdown()
end

local oldloveerrhand = love.errhand or function() end
local function loveerrhand(err)
	shutdown()
	return oldloveerrhand(err)
end

local function loveload()

	console = Console { expand=true }

	console:printf("*** Server started.")

	sthr = ServerThread()

	sthr:start()

	cthr = ClientThread()

	function cthr:onwelcome()
		console:printf("*** Connected. Your ID is #%d", self.id)
		console:printf("*** %s", self.servername:gsub("\n", "\n*** "))
		console:printf("*** %s", self.serverdesc:gsub("\n", "\n*** "))
	end

	function cthr:onuserjoin(user)
		console:printf("* %s#%d joined.", user.name, user.id)
	end

	function cthr:onuserquit(user)
		console:printf("* %s#%d quit.", user.name, user.id)
	end

	function cthr:onmessage(user, ispriv, message)
		local s, e = ispriv and ">" or "<", ispriv and "<" or ">"
		console:printf("%s%s#%d%s %s", s, user.name, e, user.id, message)
	end

	function cthr:onerror(err)
		console:printf("*** ERROR: %s", err)
	end

	cthr:start()

	entry = sui.Entry {
		expand = true,
		committed = function(self)
			if cthr then
				console:printf("<%s#%d> %s",
						cthr.name ~= nil and cthr.name or "",
						cthr.id ~= nil and cthr.id or 0,
						self.text)
				cthr:sendmessage(0, self.text)
				self.text = ""
				self.index = 1
			end
		end,
	}
	entry:setfocus()

	sui.run(sui.Box {
		mode = "v",
		console,
		sui.Box {
			mode = "h",
			entry,
			sui.Button {
				text = "Send",
				activated = function()
					return entry:committed()
				end,
			},
		}
	})

	love.keypressed = lovekeypressed
	love.update = loveupdate
	love.quit = lovequit
	love.errhand = loveerrhand

end

love.load = loveload
