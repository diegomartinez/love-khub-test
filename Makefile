
.PHONY: default all love doc lint clean distclean

default: all

APPNAME = love-khub-test
APPVER = scm-0

ZIP ?= zip -u
ZIPFLAGS ?= -9

LDOC ?= ldoc
LDOCFLAGS ?=

LUACHECK ?= luacheck
LUACHECKFLAGS ?= --quiet --no-color

RM = rm -f

all: love

MAINLOVE = $(APPNAME).love

KLASS    = $(foreach f,$(shell cat lib/klass/files.lst),lib/klass/$(f))
SIMPLEUI = $(foreach f,$(shell cat lib/simpleui/files.lst),lib/simpleui/$(f))
KHUB     = $(foreach f,$(shell cat lib/khub/files.lst),lib/khub/$(f))
MAIN     = $(foreach f,$(shell cat lib/app/files.lst),lib/app/$(f))

MAINLUA = $(KLASS) $(SIMPLEUI) $(KHUB) $(MAIN) main.lua

love: $(MAINLOVE)

$(MAINLOVE): $(MAINLUA)
	$(ZIP) $(ZIPFLAGS) $(MAINLOVE) $(MAINLUA)

docs:
	cd doc && $(LDOC) $(LDOCFLAGS) .

lint:
	$(LUACHECK) $(LUACHECKFLAGS) .

clean:

distclean: clean
	$(RM) $(MAINLOVE)
